drop table if exists transactions;
create table transactions (
    id integer primary key autoincrement NOT NULL,
    crm_id text NOT NULL,
    trans_id text NOT NULL,
    trans_to_id text NOT NULL,
    time text NOT NULL,
    amount text NOT NULL,
    CONSTRAINT unique_trans_id UNIQUE (trans_id)
);

drop table if exists contact;
create table contact (
    id integer primary key autoincrement NOT NULL,
    crm_id text NOT NULL,
    name text NOT NULL,
    email text,
    phone text,
    CONSTRAINT unique_crm_id UNIQUE (crm_id)
);
CREATE INDEX IF NOT EXISTS name_idx ON contact(name);
CREATE INDEX IF NOT EXISTS email_idx ON contact(email);
CREATE INDEX IF NOT EXISTS phone_idx ON contact(phone);
