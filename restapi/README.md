# Service Qualification REST API for GraphQL demo

## REST API

Two resources:

### Customer API

```
curl http://localhost:8000/customer/<crm_id> \
-H "Accept: application/json" \
-H "Content-Type: application/json"
```

curl example:

```
curl http://localhost:8000/customer/CRM000000000005 \
-H "Accept: application/json" \
-H "Content-Type: application/json"
```

httpie example

```
http :8000/customer/CRM000000000005 -j
```

### Transaction API

```
curl http://localhost:8000/transactions/<crm_id> \
-H "Accept: application/json" \
-H "Content-Type: application/json"
```

curl example:

```
curl http://localhost:8000/transactions/CRM000000000005 \
-H "Accept: application/json" \
-H "Content-Type: application/json"
```

httpie example:

```
http :8000/transactions/CRM000000000005 -j
```

## Start App Locally

```
./run.sh
```

## Start App in Docker

### Build docker image

```
docker build -t graphqldemo-rest:latest .
```

### Run docker image

```
docker run -itd -p 8000:8000 -v /tmp/demodb:/restapi/db --name restapi graphqldemo-rest:latest
```

### Stop docker image

```
docker stop restapi
```

### Cleanup

```
docker rm -v restapi
```