# flask
from flask import Flask, request, session, g, redirect, url_for, abort, \
    render_template, flash, make_response
from flask_negotiate import consumes, produces

from urllib.parse import urlparse
import io, json, logging, os, sqlite3, sys
import testdata

app = Flask(__name__)
app.config.from_object(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'db/demo.db'),
    SECRET_KEY='development key'
))

def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db

def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

def init_db():
    """Inits the database"""
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

@app.cli.command('initdb')
def initdb_command():
    """Flask command line command to init the database."""
    init_db()
    print('initialized the database.')
    generate_test_data()
    print('generated demo data.')

def generate_test_data():
    """loads test data for the demo into the database"""

    no_records = 20000
    batch_size = 1000
    db = get_db()

    sql_transactions = """INSERT INTO transactions (crm_id, 
        trans_id, 
        trans_to_id,
        time, 
        amount) VALUES (?, ?, ?, ?, ?)"""

    rec_no = 0

    for record in testdata.generate_test_transactions(no_records):
        rec_no = rec_no + 1
        c = db.execute(sql_transactions, (record["crm_id"],
            record["trans_id"], 
            record["trans_to_id"],
            record["time"],
            record["amount"]))
        if rec_no % batch_size == 0:
            db.commit()
            print('writing medical alarm: {}'.format(rec_no))

    sql_contact = """INSERT INTO contact (crm_id,
        name, 
        email,
        phone) VALUES (?, ?, ?, ?)"""

    rec_no = 0

    for record in testdata.generate_test_contact(int(no_records/10)):
        rec_no = rec_no + 1
        c = db.execute(sql_contact, (record["crm_id"],
            record["name"], record["email"],
            record["phone"]))
        if rec_no % batch_size == 0:
            db.commit()
            print('writing contact: {}'.format(rec_no))

    print('finished generating data -> committing') 
    db.commit()
    print('committed')

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)s [%(levelname)s] %(message)s')


"""
Get the transactions
"""
@app.route("/")
def hello():
    logging.info("GET /")
    return "The API is up"

@app.route("/transactions/<crm_id>")
@produces("application/json")
def get_transactions(crm_id):
    
    logging.info("GET /transactions/{}".format(crm_id))

    db = get_db()

    c = db.execute("SELECT crm_id, trans_id, trans_to_id, time, amount FROM transactions WHERE crm_id = ?", (crm_id,))

    resp = []

    for record in c:
        resp.append({ "crm_id": record["crm_id"], 
            "trans_id": record["trans_id"],
            "trans_to_id": record["trans_to_id"],
            "time": record["time"],
            "amount": record["amount"] })

    return json.dumps(resp), 200


"""
Get the customers
"""
@app.route("/customers")
@produces("application/json")
def get_customers():
    
    logging.info("GET /customers")
    logging.info(str(dir(request)))

    logging.info("start=" + request.args.get('start'))

    limit = request.args.get('limit')
    if not limit.isdigit():
        return "limit must be a number", 400

    db = get_db()

    c = db.execute("SELECT crm_id, name, email, phone FROM contact WHERE crm_id = ? ORDER BY crm_id LIMIT ?", (crm_id, limit))

    return "", 200

"""
Get the customer
"""
@app.route("/customer/<crm_id>")
@produces("application/json")
def get_customer(crm_id):

    logging.info("GET /customer/{}".format(crm_id))

    db = get_db()

    c = db.execute("SELECT crm_id, name, email, phone FROM contact WHERE crm_id = ?", (crm_id,))

    # won't actually retrieve more than one record due to unique constraint
    row = c.fetchone()

    resp = []

    if None == row:
        return json.dumps({}), 200
    else:
        resp = { "crm_id": row["crm_id"], 
            "name": row["name"],
            "email": row["email"],
            "phone": row["phone"] }
        return json.dumps(resp), 200


if __name__ == "__main__":
    app.run()
