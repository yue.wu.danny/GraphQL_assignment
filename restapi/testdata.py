import datetime, random, string, uuid

def gen_datetime(min_year=2009, max_year=datetime.datetime.now().year):
    # generate a datetime in format yyyy-mm-dd hh:mm:ss.000000
    start = datetime.datetime(min_year, 1, 1, 00, 00, 00)
    years = max_year - min_year + 1
    end = start + datetime.timedelta(days=365 * years)
    return start + (end - start) * random.random()

def generate_test_transactions(records=20000):
    """yields a generator with x number of service qualification records in a dictionary"""

    for i in range(1, records+1):
        crm_id = ('CRM{0:012d}'.format(random.randint(0, 2000)))
        trans_id = ('TRAN{0:012d}'.format(i))
        trans_to_id = ('CRM{0:012d}'.format(random.randint(0, 2000)))
        time = gen_datetime()
        amount = random.randint(0, 1000000) / 100

        yield { "crm_id": crm_id, 
            "trans_id": trans_id, 
            "trans_to_id": trans_to_id, 
            "time": time, 
            "amount": amount }

def generate_test_contact(records=2000):
    """Generates contacts"""
    domain_name_list = ("example.org", "example.com", "mailinator.com", "trashcanmail.com")
    first_name_list = ("donald", "mickey", "minnie", "pluto", "elsa", "anna", "peppa", "george", "winnie", "christopher")
    last_name_list = ("duck", "mouse", "sloth", "donkey", "tiger", "owl", "rabbit", "piglet", "bear", "ant", "bat")

    for i in range(1, records+1):

        crm_id = ('CRM{0:012d}'.format(i))
        name = "{} {}".format(random.choice(first_name_list).title(), random.choice(last_name_list).title())
        email = _gen_random_email(string.ascii_lowercase, domain_name_list)
        phone = random.randint(13600000000, 18600000000)

        yield { "crm_id": crm_id,
            "name": name,
            "email": email,
            "phone": phone }

def _gen_random_email(local_part_char_tuple, domain_name_tuple):
    local_part = ''.join(random.choice(local_part_char_tuple) for i in range(7))
    return "{}@{}".format(local_part, random.choice(domain_name_tuple))
