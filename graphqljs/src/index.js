import DataLoader from 'dataloader';
import express from 'express';
import fetch from 'node-fetch';
import graphqlHTTP from 'express-graphql';
import GraphQLSchema from 'graphql';
import schema from './schema';

console.log('starting graphqljs app');

const BASE_URL = 'http://restapi:8000';

function getJSONFromRelativeURL(relativeURL) {
  var url = `${BASE_URL}${relativeURL}`;
  console.log('HTTP: ' + url);
  return fetch(url, { 
    headers: { 
      Accept: 'application/json', 
      'Content-Type': 'application/json' }})
  .then(res => {
    console.log('Response is ' + res.status);
    return res.json();
  });
}

// function getCustomers() {
//   console.log('getCustomers()');  
//   return getJSONFromRelativeURL(`/customers/`);
// }

function getCustomer(crmId) {
  console.log('getCustomer(' + crmId + ')');
  return getJSONFromRelativeURL(`/customer/${crmId}`);
}

function getTransaction(crmId) {
  console.log('getTransaction(' + crmId + ')');
  return getJSONFromRelativeURL(`/transactions/${crmId}`);
}

const app = express();

app.use('/graphql', graphqlHTTP(req => {
  // const customersCacheMap = new Map();
  const customerCacheMap = new Map();
  const transactionMap = new Map();

  // const customersLoader =
  //   new DataLoader(keys => Promise.all(keys.map(getCustomers)), {customersCacheMap});
  const customerLoader = 
    new DataLoader(keys => Promise.all(keys.map(getCustomer)), {customerCacheMap});
  const transactionLoader = 
    new DataLoader(keys => Promise.all(keys.map(getTransaction)), {transactionMap});


  const loaders = {
    customer: customerLoader, 
    // customers: customersLoader,
    transaction: transactionLoader,
  };

  return {
    context: {loaders},
    graphiql: true,
    schema,
    formatError: error => ({
      message: error.message,
      locations: error.locations,
      stack: error.stack
    }),
  };
}));

app.listen(
  5000,
  () => console.log('GraphQL Server running at http://localhost:5000')
);