import fetch from 'node-fetch';
import {
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
} from 'graphql';
import {
  fromGlobalId,
  globalIdField,
  nodeDefinitions,
} from 'graphql-relay';

const {
  nodeField,
  nodeInterface,
} = nodeDefinitions(
  // A method that maps from a global id to an object
  (globalId, {loaders}) => {
    const {id, type} = fromGlobalId(globalId);
    if (type === 'Customer') {
      return loaders.customer.load(id);
    }
  },
  // A method that maps from an object to a type
  (obj) => {
    if (obj.hasOwnProperty('crmId')) {
      return CustomerType;
    }
  }
);

const TransactionType = new GraphQLObjectType({
  name: 'Transaction',
  description: 'Transactions',
  fields: () => ({
    id: globalIdField('Transaction'),
    crmId: {
      type: GraphQLString,
      description: 'Customer ID',
      resolve: customer => customer.crm_id,
    },
    transId: {
      type: GraphQLString,
      description: 'Transaction ID',
      resolve: customer => customer.trans_id,
    },
    transToId: {
      type: GraphQLString,
      description: 'ID for the customer been transfer to',
      resolve: customer => customer.trans_to_id,
    },
    time: {
      type: GraphQLString,
      description: 'Time transaction is been made',
      resolve: customer => customer.time,
    },
    amount: {
      type: GraphQLString,
      description: 'Amount that is been transfered',
      resolve: customer => customer.amount,
    },
  })
})

const CustomerType = new GraphQLObjectType({
  name: 'Customer',
  description: 'A customer',
  fields: () => ({
    id: globalIdField('Customer'),
    crmId: {
      type: GraphQLString,
      description: 'Customer ID',
      resolve: customer => customer.crm_id,
    },
    name: {
      type: GraphQLString,
      description: 'Customer name',
      resolve: customer => customer.name,
    },
    email: {
      type: GraphQLString,
      description: 'Customer email',
      resolve: customer => customer.email,
    },
    phone: {
      type: GraphQLString,
      description: 'Customer phone',
      resolve: customer => customer.phone,
    },
    transactions: {
      type: new GraphQLList(TransactionType),
      description: 'Transactions for a customer',
      resolve: (obj, args, {loaders}) => {
        return loaders.transaction.load(obj.crm_id);
      }
    }
  }),
  interfaces: [nodeInterface],
});

const QueryType = new GraphQLObjectType({
  name: 'Query',
  description: 'Query for customer related data',
  fields: () => ({
    customer: {
      type: CustomerType,
      args: {
        crmId: {type: new GraphQLNonNull(GraphQLID)},
      },
      resolve: (root, args, {loaders}) => {
        console.log('=> query: customer(' + args.crmId + ')');
        return loaders.customer.load(args.crmId);
      }
    },
  }),
});

export default new GraphQLSchema({
  query: QueryType,
});
