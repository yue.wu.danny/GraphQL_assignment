# GraphQL Demo

Quick demo of GraphQL in Node.js interacting with a several REST APIs implemented in Flask.

Demo will prime the necessary test data and the graph looks a little like this.

```
                             +----------------------+
                             |                      |
                             |       /graphql       |
                             |                      |
                             +----------------------+
                                        |
                                        |
                                        |
                                        |
                                   +----v-----+      +-------------+
                                   | Customer |      | Transaction |
                                   +----------+      +-------------+
                                   | crm_id   +------+ crm_id      |
                                   | name     |      | trans_id    |
                                   | email    |      | trans_to_id |
                                   | phone    |      | time        |
                                   |          |      | amount      |
                                   +----------+      +-------------+

```

## Example Queries

### Simple hardcoded query

```
query{
  customer(crmId:"CRM000000000005"){
    id
    crmId
    name
    email
    phone
    transactions{
      id
      crmId
      transId
      transToId
      time
      amount
    }
  }
}
```

### Introspection Query

```
{
  __schema{
    types{
      name
      fields(includeDeprecated:true){
        name
        description
        args {
          name
          description
          defaultValue
        }
        type {
          kind
          name
          description
        }
      }
    }
  }
}
```

## Docker Services

The applications used in this demo utilises Docker Swarm.

### Build Docker Images

```
docker-compose build
```

### Start Docker Services

Note, this mounts `/tmp/demodb` as `/restapi/db` in the container. A very small SQLite DB will be created in your `/tmp`.

```
docker-compose up -d
```

GraphiQL is available at: http://localhost:5000/graphql

### Stop Docker Services

```
docker-compose stop
```

### Bin It

```
docker-compose down
```